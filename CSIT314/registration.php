<?php 
session_start();
require_once 'CSIT314/EntityClass/UserTable.php';
?>

<html>
<head>
<title>Registration Page</title>

</head>
<body>
      <a href="HomePage.php">Home</a>
  
		<form action="HomePage.php" method="post">
			<input type="submit" name="exit" value='Logout' />
		</form>
		<form action="healthcareorg.php" method="post">
			<input type="submit" name="back" value='Back' />
		</form>
		
</body>
  
<style>
body 
{
  background-image: url('homee.jpg');

}
</style>


<?php 

	if (isset($_POST['save'])) 
	{	
		$name 		= $_POST['name'];
		$number 	= $_POST['number'];
		$address 	= $_POST['address'];
		$password 	= $_POST['password'];
		$type 		= $_POST['role'];
		
		$RegistrationController = new RegistrationController($name,$password,$number,$address,$type);
		if (
			$RegistrationController->checkEmptyName($name) 			== false &&
			$RegistrationController->checkEmptyNum($number)			== false &&
			$RegistrationController->checkEmptyAddr($address)		== false &&
			$RegistrationController->checkEmptyPassword($password)	== false &&
			$RegistrationController->checkEmptyType($type) 			== false 
			)
		{
			if ($RegistrationController->checkUserExist() == false)
			{
				if($RegistrationController->registrationResult())
				{
					echo "<script>alert('Succeed registering')</script>";
				}
				else
				{
					echo "<script>alert('Failed registering')</script>";
				}
			}
			else
			{
				echo "<script>alert('User already exists')</script>";
			}
		}
	}
	
?>
<h2>
	<center>Registration Page</center>
</h2>
<form action="Registration.php " method="POST">
	<center>
	<table border="1">
		
		<tr>
			
			<th>
				Your name: 
			</th>
			
			<th>
				<input type="text" name="name" /><br>
			</th>
		</tr>
		
		<tr>
			<th>
				Your number: 
			</th>
			<th>
				<input type="text" name="number" /><br>
			</th>
			</tr>
		<tr>
			<th>
				Your address:
			</th>
			<th>
				<input type="text" name="address" /><br>
			</th>
		</tr>
		<tr>
			<th>
				Your password: 
			</th>
			<th>
				<input type="text" name="password" /><br>
			</th>
		</tr>
		<tr> 
			<th>
			<label for="type">Type:</label>
			</th>
			<th>
			<select id="type" name="role" size=3>
				<option value="public" selected>Public</option>
				<option value="businessowner">Business Owner</option>
				<option value="healthcareorg">Healthcare Organization</option>
				<option value="healthcarestaff">Healthcare Staff</option>
			</select>
			</th>
		</tr>

		
		<tr>
		<th>
			<input type="submit" name="save" value="Submit Application" />
		</th>
	</tr>
	
	
</table>
</center>
</form>

</body>
</html>



