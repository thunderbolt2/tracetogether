<?php
//entity
require_once 'CSIT314/EntityClass/User.php';
require_once 'CSIT314/EntityClass/AlertList.php';
require_once 'CSIT314/EntityClass/CovidList.php';
require_once 'CSIT314/EntityClass/VaccineStatus.php';
require_once 'CSIT314/EntityClass/LocationVisited.php';
require_once 'CSIT314/EntityClass/InfectedLocation.php';

//controller
require_once 'CSIT314/ControllerClass/LoginController.php';
require_once 'CSIT314/ControllerClass/AlertListController.php';
require_once 'CSIT314/ControllerClass/ViewInfectedLocationController.php';
require_once 'CSIT314/ControllerClass/ViewLocationVisitedController.php';
require_once 'CSIT314/ControllerClass/ViewVaccineStatusController.php';
require_once 'CSIT314/ControllerClass/ViewVisitorsController.php';
require_once 'CSIT314/ControllerClass/ViewCovidStatsController.php';
require_once 'CSIT314/ControllerClass/RegistrationController.php';
require_once 'CSIT314/ControllerClass/InfectedLocationController.php';
require_once 'CSIT314/ControllerClass/SuspendController.php';
require_once 'CSIT314/ControllerClass/SearchUserController.php';
require_once 'CSIT314/ControllerClass/VaccinationSatusController.php';
require_once 'CSIT314/ControllerClass/closeContactController.php';
require_once 'CSIT314/ControllerClass/InsertCovidController.php';

class UserTable {
    private $link;
    
    public function __construct() {
        $this->link = mysqli_connect("localhost","root","","admin");
    }

    public function getUserByUsername($username) {
		$sql = "SELECT * FROM userinfo WHERE username='$username'";
        $result = mysqli_query($this->link, $sql);
        $user = null;

		if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
				$pwd 		= $row['password'];
				$pNum 		= $row['number'];
				$address 	= $row['address'];
				$role 		= $row['type'];
				$user = new User($username, $pNum,$address, $pwd, $role);
			}
        }
        return $user;
    }
	public function getAllUser(){
		$sql = "SELECT * FROM userinfo";
		$result = mysqli_query($this->link, $sql);
		$userArray = array();
		while($row = mysqli_fetch_assoc($result)){
			$name = $row['username'];
			$pwd = $row['password'];
			$type = $row['type'];
			$num = $row['number'];
			$adr = $row['address'];
			$user = new User($name,$num,$adr, $pwd, $type);
			array_push($userArray,$user);
		}
		return $userArray;
	}
	
	public function insertUser($user){
		$name = $user->getUsername();
		$number = $user->getNumber();
		$address = $user->getAddress();
		$password = $user->getPassword();
		$type = $user->getRole();
		$mysqli = "INSERT INTO userinfo (username,number,address,password,type) 
		VALUES ('$name','$number','$address','$password','$type')";
		$result = mysqli_query($this->link, $mysqli);
		return $result;
	}
	
	public function suspendUser($user){
		$username = $user->getUsername();
		$number = $user->getNumber();
		$address = $user->getAddress();
		$password = $user->getPassword();
		$type = $user->getRole();
		$sql = "UPDATE userinfo SET type = 'suspend' where username = '$username'";
		$result = mysqli_query($this->link, $sql);
		return $result;
	}
	
	
	public function insertAlertList($alert){
		$name = $alert->getUsername();
		$number = $alert->getNumber();
		$ack = $alert->getAcknowledge();

		$mysqli = "INSERT INTO AlertList (username,number,acknowledge) 
		VALUES ('$name','$number','$ack')";
		$result = mysqli_query($this->link, $mysqli);
		return $result;
	}
	
    public function getAlertListByUsername($username) {
        $sql = "SELECT * FROM AlertList WHERE username='$username'";
//        $params = array('username' => $username);
        $result = mysqli_query($this->link, $sql);
        $alertUser = null;

		if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
				$ack 		= $row['acknowledge'];
				$pNum 		= $row['number'];
				$alertUser = new AlertList($username,$pNum,$ack);
			}
        }
        return $alertUser;
    }
	
		public function getAllAlertList(){
		$sql = "SELECT * FROM AlertList";
		$result = mysqli_query($this->link, $sql);
		$alertArray = array();
		while($row = mysqli_fetch_assoc($result)){
			$name = $row['username'];
			$num = $row['number'];
			$ack = $row['acknowledge'];
			$alert = new AlertList($name,$num,$ack);
			array_push($alertArray,$alert);
		}
		return $alertArray;
	}
	
    public function getVaccineStatusByUsername($username) {
        $sql = "SELECT * FROM VaccineStatus WHERE username='$username'";
        $result = mysqli_query($this->link, $sql);
        $vaccineStatus = null;

		if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
				$sts 		= $row['status'];
				$pNum 		= $row['number'];
				$vaccineStatus = new VaccineStatus($username, $pNum,$sts);
			}
        }
        return $vaccineStatus;
    }
	public function getAllVaccineStatus(){
		$sql = "SELECT * FROM VaccineStatus";
		$result = mysqli_query($this->link, $sql);
		$vaccineArray = array();
		while($row = mysqli_fetch_assoc($result)){
			$name = $row['username'];
			$num = $row['number'];
			$sts = $row['status'];
			$vStatus = new VaccineStatus($name,$num,$sts);
			array_push($vaccineArray,$vStatus);
		}
		return $vaccineArray;
	}
	
	public function updateVaccineStatus($vaccineStatus){
		$name = $vaccineStatus->getUsername();
		$number = $vaccineStatus->getNumber();
		$status = $vaccineStatus->getStatus();

		$mysqli = "Update VaccineStatus 
				set status ='$status' 
				where '$name' = VaccineStatus.username
				and $number = VaccineStatus.number";
		$result = mysqli_query($this->link, $mysqli);
		return $result;
	}
	
	public function getLocationVisitedByUsername($username) {
        $sql = "SELECT * FROM LocationVisited WHERE username='$username'";
//        $params = array('username' => $username);
        $result = mysqli_query($this->link, $sql);
        $locationVisited = null;

		if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
				$adr1		= $row['location1'];
				$adr2		= $row['location2'];
				$adr3		= $row['location3'];
				$adr4		= $row['location4'];
				$adr5		= $row['location5'];
				$locationVisited = new LocationVisited($username, $adr1, $adr2, $adr3, $adr4, $adr5);
			}
        }
        return $locationVisited;
    }
	public function getAllLocationVisited(){
		$sql = "SELECT * FROM LocationVisited";
		$result = mysqli_query($this->link, $sql);
		$lVArray = array();
		while($row = mysqli_fetch_assoc($result)){
			$name 		= $row['username'];
			$adr1		= $row['location1'];
			$adr2		= $row['location2'];
			$adr3		= $row['location3'];
			$adr4		= $row['location4'];
			$adr5		= $row['location5'];
			$lV = new LocationVisited($name, $adr1, $adr2, $adr3, $adr4, $adr5);
			array_push($lVArray,$lV); 
		}
		return $lVArray;
	}
	
	public function getAllInfectedLocation(){
		$sql = "SELECT * FROM InfectedLocations";
		$result = mysqli_query($this->link, $sql);
		$iLArray = array();
		while($row = mysqli_fetch_assoc($result)){
			$block 		= $row['block'];
			$street		= $row['street'];
			$postal		= $row['postal'];
			$iL = new InfectedLocation($block, $street, $postal);
			array_push($iLArray,$iL); 
		}
		return $iLArray;
	}
	
	public function insertInfectedLocation($InfectedLocation){
		$block = $InfectedLocation->getBlock();
		$street = $InfectedLocation->getStreet();
		$postal = $InfectedLocation->getPostal();

		$mysqli = "INSERT INTO InfectedLocations(block,street,postal) 
		VALUES ('$block','$street','$postal')";
		$result = mysqli_query($this->link, $mysqli);
		return $result;
	}
	
	public function insertcovidlist($covid){
		$name = $covid->getUsername();
		$number = $covid->getNumber();
		$cvd = $covid->getCovid();

		$mysqli = "INSERT INTO covidlist (name,number,covid) 
		VALUES ('$name','$number','$cvd')";
		$result = mysqli_query($this->link, $mysqli);
		return $result;
	}
	
	public function getAllCovidStatus(){
		$sql = "SELECT * FROM covidlist";
		$result = mysqli_query($this->link, $sql);
		$covidArray = array();
		while($row = mysqli_fetch_assoc($result)){
			$name = $row['name'];
			$num = $row['number'];
			$cvd = $row['covid'];
			$cStatus = new CovidList($name,$num,$cvd);
			array_push($covidArray,$cStatus);
		}
		return $covidArray;
	}
	
	public function getCovidListByUsername($username) {
        $sql = "SELECT * FROM CovidList WHERE name='$username'";
//        $params = array('username' => $username);
        $result = mysqli_query($this->link, $sql);
        $covidList = null;

		if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
				$name		= $row['name'];
				$number		= $row['number'];
				$covid		= $row['covid'];
				
				$covidList = new CovidList($name, $number, $covid);
			}
        }
        return $covidList;
    }
}

?>