<?php
class AlertList {
    private $username;
    private $number;
	private $acknowledge;

    public function __construct($name,$num,$ack) {
        $this->username = $name;
		$this->number = $num;
        $this->acknowledge = $ack;
    }
	
    public function getUsername() 		{ return $this->username; }
    public function getNumber()			{ return $this->number; }
    public function getAcknowledge()	{ return $this->acknowledge; }

    private function setUsername($n) 	{ $this->username = $n; }
	private function setNumber($num)	{ $this->num = $number;}
	private function setAcknowledge($ack){ $this->ack = $acknowledge; }

}
?>
