<?php
class LocationVisited {
    private $username;
	private $location1;
	private $location2;
	private $location3;
	private $location4;
	private $location5;

    public function __construct($name,$adr1,$adr2,$adr3,$adr4,$adr5) {
        $this->username = $name;
		$this->location1 = $adr1;
		$this->location2 = $adr2;
		$this->location3 = $adr3;
		$this->location4 = $adr4;
		$this->location5 = $adr5;
    }

    public function getUsername()	 	{ return $this->username; }
    public function getLocation1() 		{ return $this->location1; }
    public function getLocation2() 		{ return $this->location2; }
    public function getLocation3() 		{ return $this->location3; }
    public function getLocation4() 		{ return $this->location4; }
    public function getLocation5() 		{ return $this->location5; }

    public function setUsername($n) 	{ $this->username = $n; }
	public function setLocation1($adr)	{ $this->location1 = $adr;}
	public function setLocation2($adr)	{ $this->location2 = $adr;}
	public function setLocation3($adr)	{ $this->location3 = $adr;}
	public function setLocation4($adr)	{ $this->location4 = $adr;}
	public function setLocation5($adr)	{ $this->location5 = $adr;}
}
?>
