<?php
class InfectedLocation {
    private $block;
	private $street;
	private $postal;


    public function __construct($block,$street,$postal) 
	{
        $this->block = $block;
		$this->street = $street;
		$this->postal = $postal;
    }

    public function getBlock()	 		{ return $this->block; }
    public function getStreet() 		{ return $this->street; }
    public function getPostal()			{ return $this->postal; }


    public function setBlock($blk) 		{ $this->block = $blk; }
    public function setStreet($street) 	{ $this->street = $street; }
    public function setPostal($postal) 	{ $this->postal = $postal; }
	
}
?>
