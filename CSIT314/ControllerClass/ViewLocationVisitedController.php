<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class ViewLocationVisitedController
{
    private $username;
    private $locationVisitedArray;
    private $locationVisited;
	
    public function __construct($username) {
        $this->username = $username;
        $this->setLocationVisited();
        $this->locationVisitedArray = $this->getAllLocationVisited();
    }

    public function locationVisitedArray() 	{ return $this->locationVisitedArray; }
	
	
	public function getAllLocationVisited(){
		$usertable = new usertable();
		$locationVisitedArray = $usertable->getAllInfectedLocation();
			return $locationVisitedArray;
		}
	public function setLocationVisited(){
		$usertable = new userTable();
		$this->locationVisited= $usertable->getLocationVisitedByUsername($this->username);
	}
	public function buildTable(){
		$location1 = $this->locationVisited->getLocation1();
		$location2 = $this->locationVisited->getLocation2();
		$location3 = $this->locationVisited->getLocation3();
		$location4 = $this->locationVisited->getLocation4();
		$location5 = $this->locationVisited->getLocation5();
		
		//create table
		echo "<table border='1' style ='text-align:left;'>
			<tr>
				<th>Location 1</th>
				<th>$location1</th>
			</tr>
			<tr>
				<th>Location 2</th>
				<th>$location2</th>
			</tr>
			<tr>
				<th>Location 3</th>
				<th>$location3</th>
			</tr>
			<tr>
				<th>Location 4</th>
				<th>$location4</th>
			</tr>
			<tr>
				<th>Location 5</th>
				<th>$location5</th>
			</tr>
		";

		
		echo "</table>";
			
		}

}
?>
