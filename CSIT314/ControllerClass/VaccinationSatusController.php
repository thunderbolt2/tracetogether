<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class VaccinationSatusController
{
    private $username;
	private $number;
	private $type;

    public function __construct($name,$num,$st) {
        $this->username = $name;
		$this->number = $num;
		$this->status = $st;
    }

    public function getUsername() 	{ return $this->username; }
    public function getNumber() 	{ return $this->number; }
    public function getStatus() 	{ return $this->status; }

	
	public function checkEmptyName($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Name is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyNum($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Number is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyStatus($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Status is not selected")</script>';
			return true;
		}
		return false;
	}
	
	// if userexit = false cont register
	//else print error msg
	public function checkUserExist(){
		$userTable = new UserTable();
		if ($userTable->getUserByUsername($this->username) == null)
		{			
			return false;
		}
		else{return true;}
	}
	
	public function updateResult(){
		$userTable = new UserTable();
		$vaccineObj = new VaccineStatus($this->username,$this->number,$this->status);
		return $userTable->updateVaccineStatus($vaccineObj);
	}
}
?>
