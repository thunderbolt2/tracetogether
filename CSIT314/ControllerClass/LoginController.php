<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class LoginController
{
    private $username;
    private $password;
	private $location;

	
    public function __construct($name,$pwd) {
        $this->username = $name;
		$this->password = $pwd;
    }

    public function getUsername() 	{ return $this->username; }
    public function getPassword() 	{ return $this->password; }
    public function getLocation()	{ return $this->location; }
	public function checkEmptyUsername($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Username is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyPassword($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Password is empty")</script>';
			return true;
		}
		return false;
	}
	
	public function loginValidity($user,$password){
		if ($user == null) 
		{
			echo '<script>alert( "Username is not registered")</script>';
			return false;
		}
		else if ($password !== $user->getPassword()) 
		{
			echo '<script>alert( "Password is incorrect")</script>';
			return false;
		}
		else
		{
			$this->setLocation($user->getRole());
			return true;
		}
	}
    private function setLocation($role){
		if 		($role == "public"){
					$this->location = "http://localhost/CSIT314/public.php";}
		else if	($role == "businessowner"){
					$this->location = "http://localhost/CSIT314/businessowner.php";}
		else if	($role == "healthcarestaff"){
					$this->location = "http://localhost/CSIT314/healthcarestaff.php";}
		else if	($role == "healthcareorg"){
					$this->location = "http://localhost/CSIT314/healthcareorg.php";}
		else if	($role == "suspend"){
					$this->location = "http://localhost/CSIT314/homepage.php";}

	}

}
?>
