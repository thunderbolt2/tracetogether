<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class AlertListController
{
    private $user;
    public function __construct($user) {
        $this->user = $user;
    }

    public function getUser() 	{ return $this->user; }
	
	public function checkRole(){
		if ($this->user->getRole() == "businessowner" ||
			$this->user->getRole() == "suspend" ||
			$this->user->getRole() == "public"){
				return true;
			}
		else{return false;}
	}
	public function isSuspended(){
		if ($this->user->getRole() == "suspend")
		{
				return true;
		}
		else{return false;}
	}
	
	public function checkIfAlerted(){
		$userTable = new userTable();
		if ($userTable->getAlertListByUsername($this->user->getUsername())!=null)
		{
			return true;
		}
		else{return false;}
		
	}

}
?>
