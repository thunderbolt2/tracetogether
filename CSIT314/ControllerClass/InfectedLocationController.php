<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class InfectedLocationController
{
    private $block;
    private $street;
	private $postalCode;
	
    public function __construct($block,$street,$postalCode) {
        $this->street = $street;
		$this->block = $block;
		$this->postalCode = $postalCode;
    }

    public function getBlock() 		{ return $this->block; }
    public function getPostalCode() 	{ return $this->postalCode; }
    public function getStreet() 	{ return $this->street; }


	public function checkEmptyBlock($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Block is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyStreet($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Street is empty")</script>';
			return true;
		}
		return false;
	}

	public function checkEmptyPostalCode($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Postal is empty")</script>';
			return true;
		}
		return false;
	}
	
	public function checkInfectedLocationExist(){
		$usertable = new Usertable();
		$allInfectedLocation = $usertable->getAllInfectedLocation();
		foreach ($allInfectedLocation as $infectedLocation){
			if (
				$infectedLocation->getBlock() == $this->block 	&&
				$infectedLocation->getStreet() == $this->street ||
				$infectedLocation->getPostal() == $this-> postalCode
			)
			{
				return true;
			}
			else{return false;}
		}
	}
	public function addInfectedLocationResult(){
		$usertable = new Usertable();
		$infectedLocation = new InfectedLocation($this->block, $this->street, $this->postalCode);
		return $usertable->insertInfectedLocation($infectedLocation);
	}


}
?>
