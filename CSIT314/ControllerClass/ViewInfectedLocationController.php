<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class ViewInfectedLocationController
{
    private $infectedLocationArray;
	
    public function __construct() {
        $this->infectedLocationArray = $this->allInfectedLocation();
    }

    public function getInfectedLocationArray() 	{ return $this->infectedLocationArray; }
	
	
	public function allInfectedLocation(){
		$usertable = new usertable();
		$infectedLocationArray = $usertable->getAllInfectedLocation();
			return $infectedLocationArray;
		}
	
	public function buildTable(){
		echo "<table border='1'>
		<tr>
		<th>Block</th>
		<th>Street</th>
		<th>Postal</th>
		</tr>";

		foreach($this->infectedLocationArray as $InfectedLocation)
		{
			echo "<tr>";
			echo "<td>" . $InfectedLocation->getBlock() . "</td>";
			echo "<td>" . $InfectedLocation->getStreet() . "</td>";
			echo "<td>" . $InfectedLocation->getPostal() . "</td>";

		echo "</tr>";
		} 
		echo "</table>";
		
	}

}
?>
