<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class InsertCovidController
{
    private $username;
	private $number;
	private $covid;

    public function __construct($name,$num,$covid) {
        $this->username = $name;
		$this->number = $num;
		$this->covid = $covid;
    }

    public function getUsername() 	{ return $this->username; }
    public function getNumber() 	{ return $this->number; }
    public function getCovid() 		{ return $this->covid; }
	
	public function checkEmptyName($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Name is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyNum($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Number is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyCovid($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Covid Box is not selected")</script>';
			return true;
		}
		return false;
	}
	
	// if userexit = false cont register
	//else print error msg
	public function checkUserExist(){
		$userTable = new UserTable();
		if ($userTable->getUserByUsername($this->username) == null)
		{			
			return false;
		}
		else{return true;}
	}
	public function checkIfCovided(){
		$userTable = new userTable();
		if ($userTable->getCovidListByUsername($this->username) == null)
		{
			//not covided
			return false;
		}
		else {return true;}
		
	}
	
	public function insertResult(){
		$userTable = new UserTable();
		$cStatus = new CovidList($this->username, $this->number, $this->covid);
		return $userTable->insertcovidlist($cStatus);
	}
	


}
?>
