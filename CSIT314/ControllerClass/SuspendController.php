<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class SuspendController
{
    private $username;
	private $number;
	private $type;

    public function __construct($name,$num,$t) {
        $this->username = $name;
		$this->number = $num;
		$this->type = $t;
    }

    public function getUsername() 	{ return $this->username; }
    public function getNumber() 	{ return $this->number; }
    public function getRole() 		{ return $this->type; }

	
	public function checkEmptyName($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Name is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyNum($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Number is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyType($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Type is not selected")</script>';
			return true;
		}
		return false;
	}
	
	public function checkUserExist(){
		$userTable = new UserTable();
		if ($userTable->getUserByUsername($this->username) == null)
		{			
			return false;
		}
		else{return true;}
	}
	public function checkUserSuspended(){
		$userTable = new UserTable();
		$user = $userTable->getUserByUsername($this->username) ;
		if ($user->getRole() == "suspend"){return true;}
		else{return false;}
	}
	
	public function suspensionResult(){
		$userTable = new UserTable();
		$user = $userTable->getUserByUsername($this->username);
		return $userTable->suspendUser($user);
	}
	
	public function displayAllUserStatus(){
		$userTable = new UserTable();
		$allUser = $userTable->getAllUser();
		echo "<table border='1'>
		<tr>
		<th>Username</th>
		<th>Number</th>
		<th>type</th>
		</tr>";
		foreach($allUser as $user)
		{
			echo "<tr>";
			echo "<td>" . $user->getUsername() . "</td>";
			echo "<td>" . $user->getNumber() . "</td>";
			echo "<td>" . $user->getRole() . "</td>";

			echo "</tr>";
		}
		echo "</table>";
	}


}
?>
