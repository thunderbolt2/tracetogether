<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class ViewCovidStatsController
{
    private $allCovidStats;
	
    public function __construct() {
        $this->setAllCovidStats();
    }

    public function getAllCovidStats() 	{ return $this->allCovidStats; }
    
	public function setAllCovidStats(){
		$userTable = new UserTable();
		$covidedArray = array();
		$allCovidStatus = $userTable->getAllCovidStatus();	
		foreach($allCovidStatus as $covidStatus){
			if ($covidStatus->getCovid() == 1)
			{
				array_push($covidedArray,$covidStatus);
			}
		}
		$this->allCovidStats =$covidedArray;
	}
	
	
	public function buildTable(){
		$cStatus ="+";
		
		echo "<table border = '1' >	
			<th>Name</th>
			<th>Number</th>
			<th>Status</th>";
			
		foreach($this->allCovidStats as $covided){
			echo "<tr>";
			echo "<td>".$covided->getUsername()."</td>";
			echo "<td>" . $covided->getNumber() . "</td>";
			echo "<td style ='text-align:center'>" . $cStatus . "</td>";
			echo "</tr>";
		}
		echo "</table>";
			
	}

}
?>
