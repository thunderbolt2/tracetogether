<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class RegistrationController
{
    private $username;
    private $password;
	private $number;
	private $address;
	private $type;

	
    public function __construct($name,$pwd,$num,$addr,$t) {
        $this->username = $name;
		$this->password = $pwd;
		$this->number = $num;
		$this->address = $addr;
		$this->type = $t;
    }

    public function getUsername() 	{ return $this->username; }
    public function getPassword() 	{ return $this->password; }
    public function getNumber() 	{ return $this->number; }
    public function getAddress() 	{ return $this->address; }
    public function getRole() 		{ return $this->type; }

	
	public function checkEmptyName($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Name is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyNum($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Number is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyAddr($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Address is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyPassword($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Password is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyType($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Type is not selected")</script>';
			return true;
		}
		return false;
	}
	public function checkUserExist(){
		$userTable = new UserTable();
		if ($userTable->getUserByUsername($this->username) !=null){
			return true;
		}
		else{return false;}
	}
	public function registrationResult(){
		$user = new User(	$this->username,$this->number,$this->address,
							$this->password,$this->type);
		$usertable = new Usertable();
		return $usertable->insertUser($user);

	}


}
?>
