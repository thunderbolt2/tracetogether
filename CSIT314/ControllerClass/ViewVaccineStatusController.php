<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class ViewVaccineStatusController
{
    private $username;
    private $vObj;
	
    public function __construct($username) {
        $this->setVaccineObj($username);
		$this->username = $username;
	}

    private function setVaccineObj($username) 	
	{
		$userTable = new UserTable();
		 $this->vObj= $userTable->getVaccineStatusByUsername($username);
	}
    public function getVaccineObj() 	{ return $this->vObj; }
    public function getUsername() 	{ return $this->username; }
    public function checkVaccinated() 	
	{
		if ($this->vObj->getStatus() ==1){
			return true; 
		}
		else{return false;}
	}

}
?>
