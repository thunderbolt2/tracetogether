<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class CloseContactController
{
    private $username;
	private $number;
	private $acknowledge;

    public function __construct($name,$num,$ack) {
        $this->username = $name;
		$this->number = $num;
		$this->acknowledge = $ack;
    }

    public function getUsername() 	{ return $this->username; }
    public function getNumber() 	{ return $this->number; }
    public function getRole() 		{ return $this->acknowledge; }
	
	public function checkEmptyName($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Name is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyNum($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Number is empty")</script>';
			return true;
		}
		return false;
	}
	public function checkEmptyAck($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Acknowledge is not selected")</script>';
			return true;
		}
		return false;
	}
	
	// if userexit = false cont register
	//else print error msg
	public function checkUserExist(){
		$userTable = new UserTable();
		if ($userTable->getUserByUsername($this->username) == null)
		{			
			return false;
		}
		else{return true;}
	}
	
	//got issue
	public function checkUserAlerted(){
		$userTable = new UserTable();
		if ($userTable->getAlertListByUsername($this->username) != null)
		{
			return true;
		}
		else {return false;}
	}
	
	public function insertResult(){
		$userTable = new UserTable();
		$alert =  new AlertList($this->username,$this->number,$this->acknowledge);
		if ($alert != null){
			return $userTable->insertAlertList($alert);
		}
		else {return false;}
	}
}
?>
