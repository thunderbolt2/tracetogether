<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class SearchUserController
{
    private $username;
    private $locationVisitedObj;
    private $userObj;
    private $vaccineStatusObj;
	

	
    public function __construct($username) {
        $this->username = $username;
		$this->setUserObj($username);
		$this->setLocationVisitedObj($username);
		$this->setVaccineStatusObj($username);
    }

    public function getUsername() 			{ return $this->username; }
    public function getLocationVisitedObj() { return $this->locationVisitedObj; }
    public function getVaccineStatusObj() 	{ return $this->vaccineStatusObj; }
    public function getUserObj() 			{ return $this->userObj; }


	private function setUsername($username){
		$this->username = $username;
	}
	private function setLocationVisitedObj($username){
		$userTable = new UserTable();
		$this->locationVisitedObj =$userTable->getLocationVisitedByUsername($username);
	}
	private function setVaccineStatusObj($username){
		$userTable = new UserTable();
		$this->vaccineStatusObj = $userTable->getVaccineStatusByUsername($username);
	}
	private function setUserObj($username){
		$userTable = new UserTable();
		$this->userObj = $userTable->getUserByUsername($username);
	}
	
	public function checkEmptySearchBar($var){
		if ($var == "" || $var == null){
			echo '<script>alert("Search Bar is empty")</script>';
			return true;
		}
		return false;
	}
		
	public function checkUserExist(){
		$userTable = new UserTable();
		if ($userTable->getUserByUsername($this->username) !=null){
			return true;
		}
		elseif ($userTable->getUserByUsername($this->username) ==null)
		{return false;}
	}
	
	public function display(){
		$username  = $this->userObj->getUsername();
		$location1 = $this->locationVisitedObj->getLocation1();
		$location2 = $this->locationVisitedObj->getLocation2();
		$location3 = $this->locationVisitedObj->getLocation3();
		$location4 = $this->locationVisitedObj->getLocation4();
		$location5 = $this->locationVisitedObj->getLocation5();
		$msg = "";
		if($this->vaccineStatusObj->getStatus() == 1) 
		{ 
			$msg = "User is vaccinated";	
		}
		else 
		{
			$msg = "User not vaccinated";
		}
		
		echo "<table border='1' style ='text-align:left;'>
		<tr >
			<th>Name:</th>
			<th>$username</th>
		</tr>
		<tr>
			<th>Location 1</th>
			<th>$location1</th>
		</tr>
		<tr>
			<th>Location 2</th>
			<th>$location2</th>
		</tr>
		<tr>
			<th>Location 3</th>
			<th>$location3</th>
		</tr>
		<tr>
			<th>Location 4</th>
			<th>$location4</th>
		</tr>
		<tr>
			<th>Location 5</th>
			<th>$location5</th>
		</tr>
		<tr>
			<th>Vaccine Status:</th>
			<th>$msg</th>
		</tr>
	";
		echo "</table>";

	}


}
?>
