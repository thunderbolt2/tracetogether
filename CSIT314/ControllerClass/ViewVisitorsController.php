<?php
require_once 'CSIT314/EntityClass/UserTable.php';

class ViewVisitorsController
{
    private $vistorsArray;
	private $user;
	
    public function __construct($username) {
		$userTable = new UserTable();
		$this->user = $userTable->getUserByUsername($username);
		$this->setVistorsArray();
    }
    public function getVistorsArray() 	{ return $this->vistorsArray; }
    public function getUser() 			{ return $this->user; }
	
	private function setVistorsArray(){
		$usertable = new usertable();
		$address = $this->user->getAddress(); 
		$allVistorsArray = $usertable->getAllLocationVisited();
		$vistorsArray =array();
		foreach ($allVistorsArray as $visitor){
			if (
				$visitor->getLocation1() ==$address ||
				$visitor->getLocation2() ==$address ||
				$visitor->getLocation3() ==$address ||
				$visitor->getLocation4() ==$address ||
				$visitor->getLocation5() ==$address 
			)
			array_push($vistorsArray,$visitor);
		}
		$this->vistorsArray = $vistorsArray;
	}
	
	public function buildTable($user){
		$username = $user->getUsername();
		$number = $user->getNumber();
		$role = $user->getRole();
		echo "<tr>";
		echo "<td>$username</td>";
		echo "<td>$number</td>";
		echo "<td>$role</td>";
		echo "</tr>";
	
	}

}
?>
