<?php
require_once 'CSIT314/EntityClass/UserTable.php';
$conn = mysqli_connect("localhost","root","","admin");

// from cd C:\xampp\htdocs\CSIT314
// run phpunit test\loginTest.php

class loginTest extends PHPUnit_Framework_TestCase{
	
	public function testLoginWork(){
		// Arnold Hanson
		
		$conn = mysqli_connect("localhost","root","","admin");
		$username = "Arnold Hanson";
		$password = "Arnold Hanson";
		$message = "Function failure";
		
		echo "\n\n"."1)Login TEST testLoginWork"."\n";
		echo "Username: ".$username."\n";
		echo "Password: ".$password."\n";
		
		$userTable = new UserTable();
		$user = $userTable->getUserByUsername($username);
		$loginCntrl = new LoginController($username,$password);
		$this->assertTrue($loginCntrl->loginValidity($user,$loginCntrl->getPassword()),$message );	
	}
	public function testLoginWrongUsername(){
		// Arnold Hanson
		
		$conn = mysqli_connect("localhost","root","","admin");
		$username = "Arnold Hanso";
		$password = "Arnold Hanson";
		$message = "Function failure";
		
		echo "\n\n"."2)Login TEST testLoginWrongUsername"."\n";
		echo "Username: ".$username."\n";
		echo "Password: ".$password."\n";
		
		$userTable = new UserTable();
		$user = $userTable->getUserByUsername($username);
		$loginCntrl = new LoginController($username,$password);
		$this->assertTrue($loginCntrl->loginValidity($user,$loginCntrl->getPassword()),$message );	
	}
		public function testLoginWrongPW(){
		// Arnold Hanson
		
		$conn = mysqli_connect("localhost","root","","admin");
		$username = "Arnold Hanson";
		$password = "Arnold Hanso";
		$message = "Function failure";
		
		echo "\n\n"."3)Login TEST testLoginWrongPW"."\n";
		echo "Username: ".$username."\n";
		echo "Password: ".$password."\n";
		
		$userTable = new UserTable();
		$user = $userTable->getUserByUsername($username);
		$loginCntrl = new LoginController($username,$password);
		$this->assertTrue($loginCntrl->loginValidity($user,$loginCntrl->getPassword()),$message );	
	}
	
}

